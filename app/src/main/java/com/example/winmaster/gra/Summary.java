package com.example.winmaster.gra;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Summary extends AppCompatActivity
{

    TextView textViewTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        textViewTimer = findViewById(R.id.textViewTimer);

        Intent intent = getIntent();
        int result = intent.getIntExtra("timer", -1);

        textViewTimer.setText("Czas gry: " + result);

    }
}
