package com.example.winmaster.gra;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.constraint.solver.widgets.Rectangle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.Timer;

public class MainActivity extends AppCompatActivity implements SensorEventListener
{
    /*
    Zadanie stworzyc prosta gre, np but spadajacy i zabijace slimaka,
    poruszanie albo buttonami albo sensorami np accelerometr prawo - lewo

    slimak omija buta, ilosc ominietych butow - zliczana
    jak go walnie to go zabije albo straci jedno z X życ

    na koncu podsumowanie, statystyki, tabela wynikow, synchronizacja google play api
     */

    SensorManager SensorManager;
    Sensor Accelerometer;
    private long lastUpdate = -1;

    public boolean leftShake;
    public boolean rightShake;
    public int amountOfLives = 3;
    public long timer;

    Handler handler;
    int screenHeight;
    int screenWidth;
    int densityDpi;
    int fps;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        GameView gameView = new GameView(this);
        setContentView(gameView);

        handler = new Handler();
        handler.postDelayed(gameView, 100);

        SensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Accelerometer = SensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        SensorManager.registerListener((SensorEventListener) this, Accelerometer, SensorManager.SENSOR_DELAY_GAME);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        densityDpi = displayMetrics.densityDpi;
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        this.setTitle("Gra - liczba żyć: " + amountOfLives);
        timer = System.currentTimeMillis();

        fps = 5;
        //fps = 30;
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastUpdate) > 100)
            {
                lastUpdate = curTime;

                float x = event.values[0];
                float y = event.values[1];

                if(Round(x,4)>10.0000)
                {
                    //Toast.makeText(this, "Right shake detected", Toast.LENGTH_SHORT).show();
                    rightShake = true;
                    leftShake = false;
                }
                else if(Round(x,4)<-10.0000)
                {
                    //Toast.makeText(this, "Left shake detected", Toast.LENGTH_SHORT).show();
                    leftShake = true;
                    rightShake = false;
                }
                if(Round(y,4)>10.0000)
                {
                    //Toast.makeText(this, "Forward", Toast.LENGTH_SHORT).show();

                    leftShake = false;
                    rightShake = false;
                }
            }
    }

    public static float Round(float Rval, int Rpl)
    {
        float p = (float)Math.pow(10,Rpl);
        Rval = Rval * p;
        float tmp = Math.round(Rval);
        return tmp/p;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        SensorManager.registerListener(this, Accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        SensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }


    public class GameView extends SurfaceView implements Runnable
    {
        SurfaceHolder holder;
        long x = 0;
        long y = 0;
        long snailPosition = 300;


        public GameView(Context context)
        {
            super(context);
            setZOrderOnTop(true);
            holder = getHolder();
            holder.setFormat(PixelFormat.TRANSPARENT);
        }

        public void draw()
        {
           if(holder.getSurface().isValid())
           {
               Canvas canvas = holder.lockCanvas();
               Bitmap bg = BitmapFactory.decodeResource(getResources(), R.drawable.backgroundx);
               canvas.drawBitmap(bg, new Rect(0,0,bg.getWidth(), bg.getHeight()), new Rect(0,0, canvas.getWidth(), canvas.getHeight()), new Paint());

               Bitmap boot  = BitmapFactory.decodeResource(this.getResources(), R.drawable.boot);
               Bitmap snail = BitmapFactory.decodeResource(this.getResources(), R.drawable.snail);

               canvas.drawBitmap(boot, x,y, new Paint());
               canvas.drawBitmap(snail, snailPosition, screenHeight -370, new Paint());

               holder.unlockCanvasAndPost(canvas);
           }

        }

        public void getCoordinateX()
        {
            x = Math.round(Math.random()*(screenWidth-120));
        }

        @Override
        public void run()
        {
            draw();

            //zmiana parametrow, odczyt pozycji gracza trzeba odczytac szerokosc i wysokosc ekranu
            y+=5;

            if(y>screenHeight)
            {
                y = 0;
               getCoordinateX();
            }

            if(leftShake)
            {
                snailPosition = 0;
            }
            if(rightShake)
            {
                snailPosition = screenWidth -200;
            }
            if(!leftShake && !rightShake)
            {
                snailPosition = 300;
            }

            if( y > (screenHeight -450) && ( (x >= snailPosition && x < (snailPosition +200)) || (x<snailPosition && x +200>snailPosition) ))
            {
                Toast.makeText(getApplicationContext(), "Bum!", Toast.LENGTH_SHORT).show();
                if(amountOfLives >0)
                {
                    amountOfLives--;
                    setTitle("Gra - liczba żyć: " + amountOfLives);
                    y =0;
                    getCoordinateX();
                }
                else
                {
                    Intent intent = new Intent(getContext(), Summary.class);
                    long end = System.currentTimeMillis();
                    intent.putExtra("timer", end-timer);
                    startActivity(intent);

                    return;
                }
            }


            handler.postDelayed(this, fps);
        }
    }
}
